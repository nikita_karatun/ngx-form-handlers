# NgxFormHandlers

A collection of several classes encapsulating common request and form submission logic for Angular 12.2.0.

<table>
<tr>
<td>Handler</td>
<td>Description</td>
</tr>
<tr>
<td>NgxRequestSubmissionHandler</td>
<td>Tracks 'is being submitted' status of the async callback, e.g. request submission.</td>
</tr>
<tr>
<td>NgxCreateFormSubmissionHandler</td>
<td>Tracks 'is being submitted' and 'ready for submission' form statuses.</td>
</tr>
<tr>
<td>NgxUpdateFormSubmissionHandler</td>
<td>Tracks 'is being submitted', 'ready for submission' and 'updates pending' update form statuses.</td>
</tr>
</table>

The handlers have both synchronous flags as well as async observable counterparts for the on push change detection strategy components.

The following secions discuss the handlers in more details and provide some usage examples.

## NgxRequestSubmissionHandler

The simplest most basic handler that tracks being submitted flag for general request.

```typescript

class CommentsComponent {
  private comments: Comment[] = [];
  
  readonly commentsUpdateHandler = new NgxDefaultRequestSubmissionHandler();
  
  constructor(private commentStore: CommentStore) {}
  
  async updateComments() {
    this.comments = await this.commentsUpdateHandler.submit(() => this.commentStore.getAll().toPromise());
  }
  
  /* rest of the code omitted for brevity, see example-app sub-project source code */
}

```

The flags set by the handler may then be used for enabling and disabling the buttons and progress bars.

```html

<div *ngIf="commentsUpdateHandler.isBeingSubmitted$() | async"
     [ngClass]="['loading-wrapper', comments.length ? '' : 'white-bg']">
  <mat-spinner></mat-spinner>
</div>
  
  
<button mat-icon-button aria-label="Edit" (click)="openUpdateDialog(comment)"
        [disabled]="!(commentsUpdateHandler.isReadyForSubmission$() | async)">
  <mat-icon>edit</mat-icon>
</button>
  
<!-- rest of the code omitted for brevity, see example-app sub-project source code -->
```

An error handler may be injected into the hander to handle the errors in the generalized manner.

## NgxCreateFormSubmissionHandler

A create form handler which mostly repeats an interface of the base request submission handler.

The difference is, the form validity is taken into account for the `isReadyForSubmission()` flag,
 i.e. the flag returns true when the form is valid and not being submitted at the moment.
 
 ```typescript
 class  CreateCommentDialogComponent {
  readonly nameFormControl: FormControl;
  readonly messageFormControl: FormControl;

  private readonly formGroup: FormGroup;

  readonly formHandler: NgxFormSubmissionHandler;
  
  constructor(formBuilder: FormBuilder,
              private commentStore: CommentStore,
              private matDialogRef: MatDialogRef<CreateCommentDialogComponent>,
              ) {
    this.nameFormControl = formBuilder.control('', [Validators.required]);
    this.messageFormControl = formBuilder.control('', [Validators.required]);
    this.formGroup = formBuilder.group({
      name: this.nameFormControl,
      message: this.messageFormControl,
    });

    this.formHandler = new NgxCreateFormSubmissionHandler(this.formGroup);
  }

  async comment() {
    await this.formHandler.submit(async () => {
      await this.commentStore.create({
        author: this.nameFormControl.value,
        message: this.messageFormControl.value,
      })
        .toPromise();
    });
    this.matDialogRef.close(true);
  }
  
  /* rest of the code omitted for brevity, see example-app sub-project source code */
}
```

As with the basic request handler, `isBeingSubmitted()` and `isReadyForSubmission()` are used for enabling and disabling
a submit button and progress bar or spinner.

A form error handler may be injected into the hander to handle the errors in the generalized manner.

## NgxUpdateFormSubmissionHandler

Update form handler that keeps track of original / last submitted values and provides `updatesArePending()` flag.

```typescript

export class UpdateCommentDialogComponent {
  readonly nameFormControl: FormControl;
  readonly messageFormControl: FormControl;

  private readonly formGroup: FormGroup;

  readonly formHandler: NgxUpdateFormSubmissionHandler;

  private commentId: number;

  constructor(formBuilder: FormBuilder,
              private commentStore: CommentStore,
              private matDialogRef: MatDialogRef<CreateCommentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data: UpdateCommentDialogData,
  ) {
    this.commentId = data.comment.id!;

    this.nameFormControl = formBuilder.control(data.comment.author, [Validators.required]);
    this.messageFormControl = formBuilder.control(data.comment.message, [Validators.required]);
    this.formGroup = formBuilder.group({
      name: this.nameFormControl,
      message: this.messageFormControl,
    });

    this.formHandler = new NgxUpdateFormSubmissionHandler(this.formGroup);
  }

  async comment() {
    await this.formHandler.submit(async () => {
      await this.commentStore.update({
        id: this.commentId,
        author: this.formHandler.getControlHandler(this.nameFormControl)!.getUpdatedValueOrUndefined(),
        message: this.formHandler.getControlHandler(this.messageFormControl)!.getUpdatedValueOrUndefined(),
      })
        .toPromise();
    });
    this.matDialogRef.close(true);
  }
  
  /* rest of the code omitted for brevity, see example-app sub-project source code */
}
```

The form handler stores a control handler for each form control to keep track of update status for each control individually.

In the example above a control handler method `getUpdatedValueOrUndefined()` used to set the value only if an update is pending.

> In this case following contract used: backend does not update a field if no value specified for the field.

## Example Application

A simple example application using the form handlers is provided in projects/example-app directory.

To launch the example application run the following from the project root.

```bash
./node_modules/.bin/ng s example-app
```
