import {merge, Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';

export function mergeFlags(observables: Observable<boolean>[], flagProducer: () => boolean): Observable<boolean> {
  let currentValue: boolean | undefined = undefined;
  return merge(... observables)
    .pipe(
      map(() => flagProducer()),
      filter((value: boolean) => {
        let toBeLetThrough = currentValue !== value;
        if (toBeLetThrough) {
          currentValue = value;
        }
        return toBeLetThrough;
      })
    );
}
