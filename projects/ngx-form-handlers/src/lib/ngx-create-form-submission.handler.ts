import {NgxRequestSubmissionHandler} from './ngx-request-submission.handler';
import {FormGroup} from '@angular/forms';
import {NgxFormSubmissionErrorHandler} from './ngx-form-submission-error.handler';
import {NgxDefaultRequestSubmissionHandler} from './ngx-default-request-submission.handler';
import {NgxFormSubmissionHandler} from './ngx-form-submission.handler';
import {merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export class NgxNoopFormSubmissionErrorHandler extends NgxFormSubmissionErrorHandler {
  override async handleError(e: any, formGroup: FormGroup): Promise<void> {
    console.error(e);
  }
}

export class NgxCreateFormSubmissionHandler extends NgxFormSubmissionHandler {
  private readonly requestSubmissionHandler: NgxRequestSubmissionHandler;

  constructor(private formGroup: FormGroup,
              private errorHandler: NgxFormSubmissionErrorHandler = new NgxNoopFormSubmissionErrorHandler()) {
    super();

    this.requestSubmissionHandler = new NgxDefaultRequestSubmissionHandler();
  }

  override getFormGroup(): FormGroup {
    return this.formGroup;
  }

  override isBeingSubmitted(): boolean {
    return this.requestSubmissionHandler.isBeingSubmitted();
  }

  override isBeingSubmitted$(): Observable<boolean> {
    return this.requestSubmissionHandler.isBeingSubmitted$();
  }

  /**
   * @return true if form is valid and not being submitted.
   */
  override isReadyForSubmission(): boolean {
    return this.requestSubmissionHandler.isReadyForSubmission() && this.formGroup.valid;
  }

  override isReadyForSubmission$(): Observable<boolean> {
    return merge(
      this.formGroup.valueChanges.pipe(map(() => {})),
      this.requestSubmissionHandler.isReadyForSubmission$().pipe(map(() => {}))
    )
      .pipe(map(() => this.isReadyForSubmission()));
  }

  /**
   * Submits the form, keeping track of the submission state.
   *
   * @param callback Asynchronous callback that actually submits the form values.
   */
  override async submit<T>(callback: () => Promise<T>): Promise<T> {
    try {
      return await this.requestSubmissionHandler.submit(callback);
    } catch (e) {
      await this.errorHandler.handleError(e, this.formGroup);
      throw e;
    }
  }
}
