import {AbstractControl, FormGroup,} from '@angular/forms';
import {NgxFormSubmissionErrorHandler} from './ngx-form-submission-error.handler';
import {NgxNoopFormSubmissionErrorHandler} from './ngx-create-form-submission.handler';
import {BehaviorSubject, Observable} from 'rxjs';
import {mergeFlags} from './merge-flags';
import {NgxFormSubmissionHandler} from './ngx-form-submission.handler';

/**
 * Form control handler that stores last submitted value, allowing it to tell if updated value is to be submitted.
 */
export abstract class NgxUpdateFormControlHandler {
  /**
   * @return last submitted value.
   */
  abstract getCurrentValue(): any;

  /**
   * Updates last submitted and control value.
   */
  abstract setCurrentValue(currentValue: any): void;

  /**
   * @return true if current value differs from last submitted one.
   */
  abstract updateIsPending(): boolean;

  /**
   * @return value if update is pending or undefined.
   */
  abstract getUpdatedValueOrUndefined(): any;
}

class NgxDefaultUpdateFormControlHandler extends NgxUpdateFormControlHandler {
  private currentValue: any;
  override getCurrentValue() {
    return this.currentValue;
  }

  override setCurrentValue(value: any) {
    this.currentValue = value;
    this.control.setValue(value);
  }

  constructor(private control: AbstractControl) {
    super();
    this.resetCurrentValue();
  }

  override updateIsPending() {
    return this.control.value !== this.currentValue;
  }

  resetCurrentValue() {
    this.currentValue = this.control.value;
  }

  override getUpdatedValueOrUndefined() {
    return this.updateIsPending() ? this.control.value : undefined;
  }
}

/**
 * Form control that stores last submitted values, allowing it to tell if updated values are to be submitted.
 */
export class NgxUpdateFormSubmissionHandler extends NgxFormSubmissionHandler {
  private beingSubmitted$ = new BehaviorSubject(false);
  private readonly formControlHandlers = new Map<AbstractControl, NgxDefaultUpdateFormControlHandler>();

  constructor(private formGroup: FormGroup,
              private errorHandler: NgxFormSubmissionErrorHandler = new NgxNoopFormSubmissionErrorHandler()) {
    super();
    this.initializeFormHandlers(formGroup);
  }

  override getFormGroup(): FormGroup {
    return this.formGroup;
  }

  override isBeingSubmitted(): boolean {
    return this.beingSubmitted$.getValue();
  }

  override isBeingSubmitted$(): Observable<boolean> {
    return this.beingSubmitted$.asObservable();
  }

  /**
   * @return true if form is valid, updates are pending and form is not being submitted.
   */
  override isReadyForSubmission(): boolean {
    return !this.beingSubmitted$.getValue() && this.formGroup.valid && this.updatesArePending();
  }

  override isReadyForSubmission$(): Observable<boolean> {
    return mergeFlags([this.beingSubmitted$, this.formGroup.valueChanges], () => this.isReadyForSubmission());
  }

  /**
   * @return true if at least one of the form input values differs from the last submitted one.
   */
  updatesArePending(): boolean {
    let updateIsPending = false;
    for (let [, handler] of this.formControlHandlers) {
      if (handler.updateIsPending()) {
        updateIsPending = true;
        break;
      }
    }
    return updateIsPending;
  }

  /*
   * @return a multicasting observable that emits every time a 'updates are pending' flag changes its value.
   */
  updatesArePending$(): Observable<boolean> {
    return mergeFlags([this.beingSubmitted$, this.formGroup.valueChanges], () => this.updatesArePending());
  }

  /**
   * Submits the form and, keeping track of the submission flags. Resets the last submitted values on successful submission.
   *
   * @param callback Asynchronous callback that actually submits the form values.
   */
  override async submit<T>(callback: () => Promise<T>): Promise<T> {
    try {
      this.beingSubmitted$.next(true);
      const result = await callback();
      this.formControlHandlers.forEach((handler: NgxDefaultUpdateFormControlHandler) => {
        handler.resetCurrentValue();
      });
      return result;
    } catch (e) {
      this.errorHandler.handleError(e, this.formGroup);
      throw e;
    } finally {
      this.beingSubmitted$.next(false);
    }
  }

  /**
   * @return update control handler for the specified form control.
   */
  getControlHandler(control: AbstractControl): NgxUpdateFormControlHandler | undefined {
    return this.formControlHandlers.get(control);
  }

  private initializeFormHandlers(formGroup: FormGroup) {
    for (let key in formGroup.controls) {
      const control: AbstractControl = formGroup.controls[key];
      if (control instanceof FormGroup) {
        this.initializeFormHandlers(control);
      } else {
        const controlHandler = new NgxDefaultUpdateFormControlHandler(control);
        this.formControlHandlers.set(control, controlHandler);
      }
    }
  }
}

