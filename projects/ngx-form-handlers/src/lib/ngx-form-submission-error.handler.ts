import {FormGroup} from '@angular/forms';

/**
 * Handles error thrown by the form submission handler's submit callback.
 */
export abstract class NgxFormSubmissionErrorHandler {
  abstract handleError(error: any, formGroup: FormGroup): Promise<void>;
}
