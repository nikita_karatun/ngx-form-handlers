import {
  NgxDefaultRequestSubmissionHandler,
  NgxRequestSubmissionErrorHandler
} from './ngx-default-request-submission.handler';
import {NgxRequestSubmissionHandler} from './ngx-request-submission.handler';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

function delay(milliseconds: number) {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
}

export class BusyWaitBlocker {
  private blocked = false;

  block() {
    this.blocked = true;
  }

  release() {
    this.blocked = false;
  }

  async wait() {
    while (this.blocked) {
      await delay(100);
    }
  }
}

export class TestErrorHandler extends NgxRequestSubmissionErrorHandler {
  lastError: any;

  override async handleError(error: any): Promise<void> {
    this.lastError = error;
  }
}

describe('NgxDefaultRequestSubmissionHandler', () => {
  let blocker: BusyWaitBlocker;
  let errorHandler: TestErrorHandler;
  let sut: NgxRequestSubmissionHandler;

  let destroy$: Subject<void>;

  let isReadyForSubmission: boolean;
  let isBeingSubmitted: boolean;

  beforeEach(() => {
    blocker = new BusyWaitBlocker();
    errorHandler = new TestErrorHandler();
    sut = new NgxDefaultRequestSubmissionHandler(errorHandler);

    destroy$ = new Subject();

    sut.isReadyForSubmission$()
      .pipe(takeUntil(destroy$))
      .subscribe((ready: boolean) => isReadyForSubmission = ready);
    sut.isBeingSubmitted$()
      .pipe(takeUntil(destroy$))
      .subscribe((being: boolean) => isBeingSubmitted = being);

    expect(sut.isReadyForSubmission()).toBeTrue();
    expect(sut.isBeingSubmitted()).toBeFalse();

    expect(isReadyForSubmission).toBeTrue();
    expect(isBeingSubmitted).toBeFalse();
  });

  afterEach(() => {
    expect(sut.isReadyForSubmission()).toBeTrue();
    expect(sut.isBeingSubmitted()).toBeFalse();

    expect(isReadyForSubmission).toBeTrue();
    expect(isBeingSubmitted).toBeFalse();

    destroy$.next();
    destroy$.complete();
  });

  it('should update flags on submit / receive', async () => {
    const promise = sut.submit(async () => {
      blocker.block();
      return 1;
    });

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeTrue();

    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeTrue();

    blocker.release();
    expect(await promise).toEqual(1);
  });

  it('should update flags on error thrown', async () => {
    const promise = sut.submit(async () => {
      blocker.block();
      throw Error('request failed');
    });

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeTrue();

    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeTrue();

    blocker.release();

    let error: Error | null = null;
    try {
      await promise;
    } catch (e) {
      error = e;
    }
    expect(error!.message).toEqual('request failed');
    expect(errorHandler.lastError).toEqual(error);
  });
});
