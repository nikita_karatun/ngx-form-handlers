import {NgxRequestSubmissionHandler} from './ngx-request-submission.handler';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export abstract class NgxRequestSubmissionErrorHandler {
  abstract handleError(error: any): Promise<void>;
}

export class NgxNoopRequestSubmissionErrorHandler extends NgxRequestSubmissionErrorHandler {
  override async handleError(error: any): Promise<void> {}
}

export class NgxDefaultRequestSubmissionHandler extends NgxRequestSubmissionHandler {
  private beingSubmitted$ = new BehaviorSubject(false);

  constructor(private errorHandler: NgxRequestSubmissionErrorHandler = new NgxNoopRequestSubmissionErrorHandler()) {
    super();
  }

  override isBeingSubmitted(): boolean {
    return this.beingSubmitted$.getValue();
  }

  override isBeingSubmitted$(): Observable<boolean> {
    return this.beingSubmitted$.asObservable();
  }

  override isReadyForSubmission(): boolean {
    return !this.beingSubmitted$.getValue();
  }

  override isReadyForSubmission$(): Observable<boolean> {
    return this.beingSubmitted$
      .pipe(
        map((beingSubmitted: boolean) => !beingSubmitted)
      );
  }

  override async submit<T>(callback: () => Promise<T>): Promise<T> {
    try {
      this.beingSubmitted$.next(true);
      return await callback();
    } catch (e) {
      this.errorHandler.handleError(e);
      throw e;
    } finally {
      this.beingSubmitted$.next(false);
    }
  }
}
