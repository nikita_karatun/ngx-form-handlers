import {FormGroup} from '@angular/forms';
import {NgxRequestSubmissionHandler} from './ngx-request-submission.handler';

/**
 * Tracks the state of the form being submitted.
 */
export abstract class NgxFormSubmissionHandler extends NgxRequestSubmissionHandler {
  /**
   * @return form group that controls the form being handled.
   */
  abstract getFormGroup(): FormGroup;
}


