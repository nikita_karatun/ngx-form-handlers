import {Observable} from 'rxjs';

/**
 * Tracks the state of the request being submitted.
 */
export abstract class NgxRequestSubmissionHandler {

  /**
   * @return true if submit method is being executed.
   */
  abstract isBeingSubmitted(): boolean;

  /**
   * @return a multicasting observable that emits every time a 'is being submitted' flag changes its value.
   */
  abstract isBeingSubmitted$(): Observable<boolean>;

  /*
   * @return true if ready for the submission.
   */
  abstract isReadyForSubmission(): boolean;

  /*
   * @return a multicasting observable that emits every time a 'is ready for submission' flag changes its value.
   */
  abstract isReadyForSubmission$(): Observable<boolean>;

  /**
   * Sets isBeingSubmitted flag to true, executes callback and resets isBeingSubmitted flag to false.
   */
  abstract submit<T>(callback: () => Promise<T>): Promise<T>;
}
