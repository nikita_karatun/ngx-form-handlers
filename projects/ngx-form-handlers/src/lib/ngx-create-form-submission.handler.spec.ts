import {NgxCreateFormSubmissionHandler} from './ngx-create-form-submission.handler';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxFormSubmissionErrorHandler} from './ngx-form-submission-error.handler';
import {NgxFormSubmissionHandler} from './ngx-form-submission.handler';
import {BusyWaitBlocker} from './ngx-default-request-submission.handler.spec';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export class TestFormSubmissionErrorHandler extends NgxFormSubmissionErrorHandler {
  lastError?: Error;

  override async handleError(e: any, formGroup: FormGroup): Promise<void> {
    console.error(e);
    this.lastError = e;
  }
}

describe('NgxCreateFormSubmissionHandler', () => {
  let blocker: BusyWaitBlocker;
  let formGroup: FormGroup;
  let errorHandler: TestFormSubmissionErrorHandler;
  let sut: NgxFormSubmissionHandler;

  let destroy$: Subject<void>;

  let isReadyForSubmission: boolean;
  let isBeingSubmitted: boolean;

  beforeEach(() => {
    blocker = new BusyWaitBlocker();
    errorHandler = new TestFormSubmissionErrorHandler();
    formGroup = new FormGroup({
      requiredField: new FormControl('', Validators.required)
    });
    sut = new NgxCreateFormSubmissionHandler(formGroup, errorHandler);

    destroy$ = new Subject();

    sut.isReadyForSubmission$()
      .pipe(takeUntil(destroy$))
      .subscribe((ready: boolean) => isReadyForSubmission = ready);
    sut.isBeingSubmitted$()
      .pipe(takeUntil(destroy$))
      .subscribe((being: boolean) => isBeingSubmitted = being);

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeFalse();

    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeFalse();

    formGroup.controls.requiredField.setValue('some value');
    expect(sut.isReadyForSubmission()).toBeTrue();
    expect(sut.isBeingSubmitted()).toBeFalse();

    expect(isReadyForSubmission).toBeTrue();
    expect(isBeingSubmitted).toBeFalse();
  });

  afterEach(() => {
    expect(sut.isReadyForSubmission()).toBeTrue();
    expect(sut.isBeingSubmitted()).toBeFalse();

    expect(isReadyForSubmission).toBeTrue();
    expect(isBeingSubmitted).toBeFalse();

    destroy$.next();
    destroy$.complete();
  });

  it('should update flags on submit / receive', async () => {
    const promise = sut.submit(async () => {
      blocker.block();
      return 1;
    });

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeTrue();

    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeTrue();

    blocker.release();

    expect(await promise).toEqual(1);
  });

  it('should update flags on error thrown', async () => {
    const promise = sut.submit(async () => {
      blocker.block();
      throw Error('request failed');
    });

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeTrue();

    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeTrue();

    blocker.release();

    let error: Error | null;
    try {
      await promise;
    } catch (e) {
      error = e;
    }
    expect(error!.message).toEqual('request failed');
    expect(errorHandler.lastError!.message).toEqual('request failed');
  });
});
