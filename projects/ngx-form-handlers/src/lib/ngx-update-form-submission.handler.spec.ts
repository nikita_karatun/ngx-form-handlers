import {NgxUpdateFormSubmissionHandler} from './ngx-update-form-submission.handler';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TestFormSubmissionErrorHandler} from './ngx-create-form-submission.handler.spec';
import {BusyWaitBlocker} from './ngx-default-request-submission.handler.spec';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

describe('NgxUpdateFormSubmissionHandler', () => {
  let blocker: BusyWaitBlocker;
  let formControl: FormControl;
  let formGroup: FormGroup;
  let errorHandler: TestFormSubmissionErrorHandler;

  let sut: NgxUpdateFormSubmissionHandler;

  let destroy$: Subject<void>;

  let isReadyForSubmission = false;
  let isBeingSubmitted = false;
  let updatesPending = false;

  beforeEach(() => {
    blocker = new BusyWaitBlocker();
    const formBuilder = new FormBuilder();
    formControl = formBuilder.control('original value', [Validators.required]);
    formGroup = formBuilder.group({
      requiredField: formControl
    });
    errorHandler = new TestFormSubmissionErrorHandler();
    sut = new NgxUpdateFormSubmissionHandler(formGroup, errorHandler);

    destroy$ = new Subject();

    sut.isReadyForSubmission$()
      .pipe(takeUntil(destroy$))
      .subscribe((ready: boolean) => isReadyForSubmission = ready);
    sut.isBeingSubmitted$()
      .pipe(takeUntil(destroy$))
      .subscribe((being: boolean) => isBeingSubmitted = being);
    sut.updatesArePending$()
      .pipe(takeUntil(destroy$))
      .subscribe((pending: boolean) => updatesPending = pending);
  });

  afterEach(() => {
    destroy$.next();
    destroy$.complete();
  });

  it('should be ready for submission only if current value differs from original', async () => {
    let controlHandler = sut.getControlHandler(formControl)!;

    expect(sut.isReadyForSubmission()).toBeFalse(); // no updates pending
    expect(isReadyForSubmission).toBeFalse();
    expect(sut.updatesArePending()).toBeFalse();
    expect(updatesPending).toBeFalse();
    expect(controlHandler.getUpdatedValueOrUndefined()).toBeUndefined();
    expect(controlHandler.getCurrentValue()).toEqual('original value');

    formControl.setValue('');
    expect(sut.isReadyForSubmission()).toBeFalse(); // field is required
    expect(isReadyForSubmission).toBeFalse();

    formControl.setValue('updated value');
    expect(sut.isReadyForSubmission()).toBeTrue();
    expect(isReadyForSubmission).toBeTrue();
    expect(sut.updatesArePending()).toBeTrue();
    expect(updatesPending).toBeTrue();
    expect(controlHandler.getUpdatedValueOrUndefined()).toEqual('updated value');
    expect(controlHandler.getCurrentValue()).toEqual('original value');

    await sut.submit(async () => {});

    expect(sut.isReadyForSubmission()).toBeFalse(); // no updates pending
    expect(isReadyForSubmission).toBeFalse();
    expect(sut.updatesArePending()).toBeFalse();
    expect(updatesPending).toBeFalse();
    expect(controlHandler.getUpdatedValueOrUndefined()).toBeUndefined();
    expect(controlHandler.getCurrentValue()).toEqual('updated value');

    formControl.setValue('newly updated value');
    expect(sut.isReadyForSubmission()).toBeTrue();
    expect(isReadyForSubmission).toBeTrue();
    expect(sut.updatesArePending()).toBeTrue();
    expect(updatesPending).toBeTrue();
    expect(controlHandler.getUpdatedValueOrUndefined()).toEqual('newly updated value');
    expect(controlHandler.getCurrentValue()).toEqual('updated value');
  });

  it('should update flags on submit / receive', async () => {
    const promise = sut.submit(async () => {
      blocker.block();
      return 1;
    });

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeTrue();
    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeTrue();

    blocker.release();

    expect(await promise).toEqual(1);
  });

  it('should update flags on error thrown', async () => {
    const promise = sut.submit(async () => {
      blocker.block();
      throw Error('request failed');
    });

    expect(sut.isReadyForSubmission()).toBeFalse();
    expect(sut.isBeingSubmitted()).toBeTrue();
    expect(isReadyForSubmission).toBeFalse();
    expect(isBeingSubmitted).toBeTrue();

    blocker.release();

    let error: Error | null;
    try {
      await promise;
    } catch (e) {
      error = e;
    }
    expect(error!.message).toEqual('request failed');
    expect(errorHandler.lastError!.message).toEqual('request failed');
  });

  it('should set current value', () => {
    let controlHandler = sut.getControlHandler(formControl)!;

    expect(sut.isReadyForSubmission()).toBeFalse(); // no updates pending
    expect(isReadyForSubmission).toBeFalse();
    expect(controlHandler.getUpdatedValueOrUndefined()).toBeUndefined();
    expect(controlHandler.getCurrentValue()).toEqual('original value');

    controlHandler.setCurrentValue('new value');
    expect(sut.isReadyForSubmission()).toBeFalse(); // no updates pending
    expect(isReadyForSubmission).toBeFalse();
    expect(controlHandler.getUpdatedValueOrUndefined()).toBeUndefined();
    expect(controlHandler.getCurrentValue()).toEqual('new value');
    expect(formControl.value).toEqual('new value');
  });
});
