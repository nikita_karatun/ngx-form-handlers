import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {CommentStore} from '../../stores/comment.store';
import {Comment} from '../../models/comment';
import {MatDialog} from '@angular/material/dialog';
import {CreateCommentDialogComponent} from '../create-comment-dialog/create-comment-dialog.component';
import {NgxDefaultRequestSubmissionHandler} from '../../../../../ngx-form-handlers/src/public-api';
import {
  UpdateCommentDialogComponent,
  UpdateCommentDialogData
} from '../update-comment-dialog/update-comment-dialog.component';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommentsComponent implements OnInit, AfterViewInit, OnDestroy {
  private _comments: Comment[] = [];
  get comments() {
    return this._comments;
  }

  private _beingUpdated = false;
  get beingUpdated() {
    return this._beingUpdated;
  }

  readonly commentsUpdateHandler = new NgxDefaultRequestSubmissionHandler();

  @ViewChildren("commentCard") private commentCards!: QueryList<ElementRef>;

  private destroy$ = new Subject<void>();

  private commentBeingUpdated = false;

  constructor(private commentStore: CommentStore,
              private matDialog: MatDialog,
  ) {
  }

  async ngOnInit() {
    await this.updateComments();
  }

  async ngAfterViewInit() {
    this.commentCards.changes
      .pipe(takeUntil(this.destroy$))
      .subscribe((list: QueryList<ElementRef>) => {
        if (list && list.last && !this.commentBeingUpdated) {
          list.last.nativeElement.scrollIntoView();
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  async updateComments() {
    this._comments = await this.commentsUpdateHandler.submit(() => this.commentStore.getAll().toPromise());
  }

  async openCreateDialog() {
    const created = await this.matDialog.open(CreateCommentDialogComponent, {
      panelClass: 'example-app__dialog-panel'
    })
      .afterClosed()
      .toPromise();

    if (created) {
      this.updateComments();
    }
  }

  async openUpdateDialog(comment: Comment) {
    try {
      this.commentBeingUpdated = true;
      const data: UpdateCommentDialogData = { comment };
      const updated = await this.matDialog.open(UpdateCommentDialogComponent, {
        panelClass: 'example-app__dialog-panel',
        data: data,
      })
        .afterClosed()
        .toPromise();
      if (updated) {
        this.updateComments();
      }
    } finally {
      this.commentBeingUpdated = false;
    }
  }
}
