import {ChangeDetectionStrategy, Component, Inject, Input} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxUpdateFormSubmissionHandler} from '../../../../../ngx-form-handlers/src/public-api';
import {CommentStore} from '../../stores/comment.store';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CreateCommentDialogComponent} from '../create-comment-dialog/create-comment-dialog.component';
import {Comment} from '../../models/comment'

export interface UpdateCommentDialogData {
  comment: Comment;
}

@Component({
  templateUrl: '../create-comment-dialog/create-comment-dialog.component.html',
  styleUrls: ['../create-comment-dialog/create-comment-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateCommentDialogComponent {
  readonly nameFormControl: FormControl;
  readonly messageFormControl: FormControl;

  private readonly formGroup: FormGroup;

  readonly formHandler: NgxUpdateFormSubmissionHandler;

  private commentId: number;

  @Input() get title() {
    return 'Update Comment';
  }

  @Input() get submitButtonName() {
    return 'Update';
  }

  constructor(formBuilder: FormBuilder,
              private commentStore: CommentStore,
              private matDialogRef: MatDialogRef<CreateCommentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data: UpdateCommentDialogData,
  ) {
    this.commentId = data.comment.id!;

    this.nameFormControl = formBuilder.control(data.comment.author, [Validators.required]);
    this.messageFormControl = formBuilder.control(data.comment.message, [Validators.required]);
    this.formGroup = formBuilder.group({
      name: this.nameFormControl,
      message: this.messageFormControl,
    });

    this.formHandler = new NgxUpdateFormSubmissionHandler(this.formGroup);
  }

  async comment() {
    await this.formHandler.submit(async () => {
      await this.commentStore.update({
        id: this.commentId,
        author: this.formHandler.getControlHandler(this.nameFormControl)!.getUpdatedValueOrUndefined(),
        message: this.formHandler.getControlHandler(this.messageFormControl)!.getUpdatedValueOrUndefined(),
      })
        .toPromise();
    });
    this.matDialogRef.close(true);
  }
}
