import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  NgxCreateFormSubmissionHandler,
  NgxFormSubmissionHandler
} from '../../../../../ngx-form-handlers/src/public-api';
import {CommentStore} from '../../stores/comment.store';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './create-comment-dialog.component.html',
  styleUrls: ['./create-comment-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateCommentDialogComponent {
  readonly nameFormControl: FormControl;
  readonly messageFormControl: FormControl;

  private readonly formGroup: FormGroup;

  readonly formHandler: NgxFormSubmissionHandler;

  @Input() get title() {
    return 'Create Comment';
  }

  @Input() get submitButtonName() {
    return 'Comment';
  }

  constructor(formBuilder: FormBuilder,
              private commentStore: CommentStore,
              private matDialogRef: MatDialogRef<CreateCommentDialogComponent>,
              ) {
    this.nameFormControl = formBuilder.control('', [Validators.required]);
    this.messageFormControl = formBuilder.control('', [Validators.required]);
    this.formGroup = formBuilder.group({
      name: this.nameFormControl,
      message: this.messageFormControl,
    });

    this.formHandler = new NgxCreateFormSubmissionHandler(this.formGroup);
  }

  async comment() {
    await this.formHandler.submit(async () => {
      await this.commentStore.create({
        author: this.nameFormControl.value,
        message: this.messageFormControl.value,
      })
        .toPromise();
    });
    this.matDialogRef.close(true);
  }
}
