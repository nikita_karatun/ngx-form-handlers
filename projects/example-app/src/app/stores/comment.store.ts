import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {delay, map} from 'rxjs/operators';
import {Comment} from '../models/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentStore {
  private static DELAY = 1000;

  private comments: Comment[] = [];

  getAll(): Observable<Comment[]> {
    return of([...this.comments])
      .pipe(
        delay(CommentStore.DELAY)
      );
  };

  create(comment: Comment): Observable<void> {
    return of(comment)
      .pipe(
        delay(CommentStore.DELAY),
        map((comment: Comment) => {
          this.doCreate(comment);
        }),
      );
  }

  update(params: {
    id: number,
    author: string,
    message: string,
  }): Observable<void> {
    return of(params)
      .pipe(
        delay(CommentStore.DELAY),
        map(() => {
          if (params.id) {
            throw Error('attempt to update entity without id');
          }

          const comment = {...this.comments[params.id]};
          if (params.author) {
            comment.author = params.author;
          }
          if (params.message) {
            comment.message = params.message;
          }

          this.comments[params.id] = comment;
        }),
      );
  }

  private doCreate(comment: Comment) {
    comment.id = this.comments.length;
    this.comments.push({...comment});
  }
}
