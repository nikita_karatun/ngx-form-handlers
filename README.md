# NgxFormHandlers

*NgxFormHandlers* is a collection of several classes encapsulating common request and form submission logic for Angular 12.2.0.

This is a workspace for *NgxFormHandlers* containing the *NgxFormHandlers* themselves as well as an example application.

The *NgxFormHandlers* documentation may be found [here](projects/ngx-form-handlers/README.md).
