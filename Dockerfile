FROM node:latest
  ENV DEBIAN_FRONTEND=noninteractive

  RUN apt-get update

  RUN apt-get install git wget curl ca-certificates rsync gnupg -y

  RUN wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
  RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
  RUN apt-get update
  RUN apt-get install google-chrome-stable -y

  ENV NODE_OPTIONS=--openssl-legacy-provider
